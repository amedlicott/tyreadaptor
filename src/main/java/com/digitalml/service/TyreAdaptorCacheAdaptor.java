package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;

import static net.logstash.logback.argument.StructuredArguments.*;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.ChannelSftp.LsEntrySelector;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class TyreAdaptorCacheAdaptor {

    private static final Logger logger = LoggerFactory.getLogger("tyreadaptor:12");

    private static final String cacheURL = "http://infocache-test:9200/cache";
    
    private static final String resourceName = "tyreadaptor";

    public static void main(String[] args) {
   
   		// Initialise index on startup
		Unirest.put(cacheURL).asStringAsync();
   
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
		while (check(resourceName, (file) -> {
			
			Reader reader = new InputStreamReader(file, "UTF-8");
			CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader().withIgnoreEmptyLines().withIgnoreSurroundingSpaces());
			
			int counter=1;
			// Read in JavaScript transformation script
			String evaluationScript = "var o = " + IOUtils.toString(TyreAdaptorCacheAdaptor.class.getResourceAsStream("mapping.js"), "utf-8") + "; var result = JSON.stringify(o.tyreadaptor);";
			
			// Set up JavaScript parser
			Context cx = Context.enter();
			try {
				for (final CSVRecord record : parser) {
					Scriptable scope = cx.initStandardObjects();
				    ScriptableObject.putProperty(scope, "id", counter);
					try {
					    ScriptableObject.putProperty(scope, "id", record.get("id"));
                    } catch (Exception e) {}
					try {
					    ScriptableObject.putProperty(scope, "name", record.get("name"));
                    } catch (Exception e) {}
					try {
					    ScriptableObject.putProperty(scope, "manufacturer", record.get("manufacturer"));
                    } catch (Exception e) {}
					try {
					    ScriptableObject.putProperty(scope, "width", record.get("width"));
                    } catch (Exception e) {}
					try {
					    ScriptableObject.putProperty(scope, "profile", record.get("profile"));
                    } catch (Exception e) {}
					try {
					    ScriptableObject.putProperty(scope, "rim", record.get("rim"));
                    } catch (Exception e) {}
					try {
					    ScriptableObject.putProperty(scope, "load", record.get("load"));
                    } catch (Exception e) {}
					try {
					    ScriptableObject.putProperty(scope, "sku", record.get("sku"));
                    } catch (Exception e) {}
					
					cx.evaluateString(scope, evaluationScript, "Mapping", 1, null);
					
					String infoObject = Context.toString(ScriptableObject.getProperty(scope, "result"));
					
					logger.info("Adding entry " + counter + " as " + infoObject);
					
					HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + counter++).header("accept", "application/json")
							.header("Content-Type", "application/json").body(infoObject).asString();
							
					if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299) {
					    logger.warn("When contacting " + cacheURL + "/" + resourceName + "/" + counter + " the response was: " + cacheResponse.getStatus() + " " + cacheResponse.getStatusText());
						break;
					}
					   
					logger.info("Successfully added");
				}
			} finally {
				parser.close();
				reader.close();
			}

			return true;
		})) {

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				break;
			}
		};
		
		stop();
    }
    
	@FunctionalInterface
	public interface FileImporter {
		boolean handle(InputStream file) throws Exception;
	}

	public static boolean check(final String path, final FileImporter importer) {
		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession("foo", "igniteftp", 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword("pass");
			session.connect();
            logger.debug("Connected to sftp session. Status: " + session.isConnected());
			Channel channel = session.openChannel("sftp");
			channel.connect();
			logger.debug("Connected to sftp channel. Status: " + channel.isConnected());
			ChannelSftp sftpChannel = (ChannelSftp) channel;

            logger.debug("About to create dir " + path);
			try {
				sftpChannel.mkdir("/poller/data");
			} catch (Exception e) {
			    logger.debug("Caught exception creating path /poller/data " + e);
			}
			try {
				sftpChannel.mkdir("/poller/data/" + path);
			} catch (Exception e) {
			    logger.debug("Caught exception creating path /poller/data/" + path + " " + e);
			}
			try {
				sftpChannel.cd("/poller/data/" + path);
			} catch (Exception e) {
			    logger.error("Caught exception cding path " + e);
			}

			LsEntrySelector selector = new LsEntrySelector() {

				@Override
				public int select(LsEntry arg0) {
				    logger.info("------------ GET CSV " + arg0.getFilename());
					if (arg0.getFilename().startsWith("."))
						return CONTINUE;

					try {
						if (importer.handle(sftpChannel.get(arg0.getFilename()))) {
						    logger.info("------------ IMPORTED " + arg0.getFilename());
							sftpChannel.rm(arg0.getFilename());
							return CONTINUE;
						} else
							return BREAK;

					} catch (Exception e) {
						e.printStackTrace();
						return BREAK;
					}
				}
			};
			
			// sftpChannel.ls("*.csv", selector);
			
			try {
				sftpChannel.ls("*.csv", selector);
			} catch (Exception e) {
			    logger.debug("Caught exception LS " + e);
			}

			sftpChannel.exit();
			session.disconnect();

			return true;

		} catch (JSchException e) {
		    logger.debug("==================== JSchException " + e);
			e.printStackTrace();
			return false;

		} catch (SftpException e) {
		    logger.debug("==================== SftpException " + e);
			e.printStackTrace();
			return false;
		}
	}    
}